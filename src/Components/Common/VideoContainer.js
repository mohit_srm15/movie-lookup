import React, {useEffect, useState} from 'react';
import {View, ImageBackground, TouchableOpacity} from 'react-native';
import Icon from 'react-native-vector-icons/AntDesign';
import {connect} from 'react-redux';
import AsyncStorage from '@react-native-community/async-storage';

import {LightText} from './StyledTexts';
import Color from '../../Styles/Color';
import {addShortlistData} from '../../Actions/ActionCreator';
import store from '../../Store';

function VideoContainer(props) {
  const [movieDetail, setMovieDetail] = useState({});
  const [shortList, setShortList] = useState({});

  useEffect(() => {
    if (props.movieDetail) {
      setMovieDetail(props.movieDetail);
    }
    if (props.state.shortList && Object.keys(props.state.shortList).length) {
      setShortList({...props.state.shortList});
    }
  }, [props]);

  function addToShortList() {
    let detail = {...props.state.shortList};
    detail[movieDetail.imdbID] = movieDetail;
    store.dispatch(addShortlistData(detail));
    AsyncStorage.setItem('shortlistData', JSON.stringify(detail));
  }

  function removeFromShortlist() {
    let detail = {...props.state.shortList};
    delete detail[movieDetail.imdbID];
    store.dispatch(addShortlistData(detail));
    AsyncStorage.setItem('shortlistData', JSON.stringify(detail));
  }

  return (
    <View
      style={{
        borderRadius: 20,
        marginVertical: 10,
        flex: 1,
      }}>
      <ImageBackground
        source={
          movieDetail.Poster && movieDetail.Poster !== 'N/A'
            ? {uri: movieDetail.Poster}
            : require('../../../assets/emptyImage.jpg')
        }
        style={{
          width: '100%',
          height: 150,
          borderRadius: 20,
          justifyContent: 'flex-end',
        }}
        imageStyle={{
          width: '100%',
          height: 150,
          borderWidth: 1,
          borderColor: Color.appColor2,
          borderRadius: 20,
          flex: 1,
        }}>
        <View
          style={{
            flex: 0.75,
            borderColor: 'white',
            paddingTop: 5,
            paddingHorizontal: 5,
          }}>
          <View style={{flexDirection: 'row', justifyContent: 'flex-end'}}>
            {movieDetail.imdbID && shortList[movieDetail.imdbID] ? (
              <TouchableOpacity
                onPress={() => removeFromShortlist()}
                style={{
                  backgroundColor: Color.blackShade_2,
                  padding: 5,
                  borderRadius: 10,
                  opacity: 0.9,
                }}>
                <Icon name={'check'} size={12} color={Color.appColor} />
              </TouchableOpacity>
            ) : (
              <TouchableOpacity
                onPress={() => addToShortList()}
                style={{
                  backgroundColor: Color.blackShade_2,
                  padding: 5,
                  borderRadius: 10,
                  opacity: 0.9,
                }}>
                <Icon name={'pushpino'} size={12} color={Color.appColor} />
              </TouchableOpacity>
            )}
          </View>
        </View>
        <View
          style={{
            flex: 0.35,
            justifyContent: 'center',
            borderColor: 'white',
          }}>
          <View
            style={[
              {
                paddingHorizontal: 5,
                backgroundColor: Color.blackShade_2,
                opacity: 0.6,
                borderBottomLeftRadius: 20,
                borderBottomRightRadius: 20,
                paddingVertical: 2,
                flexDirection: 'row',
                flex: 1,
                alignItems: 'center',
                borderWidth: 1,
                borderBottomColor: Color.appColor2,
                borderLeftColor: Color.appColor2,
                borderRightColor: Color.appColor2,
                borderTopColor: 'transparent',
              },
            ]}>
            <View style={{flex: 0.7}}>
              <LightText
                style={{
                  fontSize: 15,
                  color: Color.gray07,
                  letterSpacing: 2,
                }}>
                {`${
                  movieDetail.Title ? movieDetail.Title.slice(0, 15) : 'N/A'
                } - ${movieDetail.Year ? movieDetail.Year : 'N/A'}`}
              </LightText>
            </View>
            <View style={{flex: 0.05}} />
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'center',
                alignItems: 'center',
                flex: 0.25,
                marginLeft: 5,
                backgroundColor: Color.gray04,
                paddingVertical: 5,
                borderRadius: 10,
              }}>
              <LightText
                style={{
                  color: Color.lightYellow,
                  fontSize: 12,
                  marginHorizontal: 3,
                }}>
                4.5
              </LightText>
              <Icon name={'star'} color={Color.lightYellow} size={12} />
            </View>
          </View>
        </View>
      </ImageBackground>
    </View>
  );
}

const mapStateToProps = (state) => {
  return {state};
};

VideoContainer = connect(mapStateToProps)(VideoContainer);
export default VideoContainer;
