import React, {useState, useEffect} from 'react';
import {View, ScrollView} from 'react-native';

import VideoContainer from '../Common/VideoContainer';
import {searchMovie} from '../../API';
import {LightText} from '../Common/StyledTexts';

function SearchList(props) {
  let {searchKeyword} = props;
  const [movieList, setMovieList] = useState([]);

  useEffect(() => {
    getMovieList();
  }, [searchKeyword]);

  async function getMovieList() {
    try {
      let movieResp = await searchMovie(searchKeyword);
      if (movieResp) {
        setMovieList(movieResp);
      }
    } catch (error) {
      console.log(' error ', error);
    }
  }

  return (
    <View style={{flex: 1}}>
      <ScrollView style={{flex: 1, padding: 10, marginVertical: 10}}>
        <View style={{flexDirection: 'row', flexWrap: 'wrap'}}>
          {movieList.length > 0 ? (
            movieList.map((movie, i) => (
              <View
                key={`movie-${i}`}
                style={{width: '50%', borderWidth: 1, paddingHorizontal: 10}}>
                <VideoContainer movieDetail={movie} />
              </View>
            ))
          ) : (
            <View style={{padding: 20}}>
              <LightText>Movie not found</LightText>
            </View>
          )}
        </View>
      </ScrollView>
    </View>
  );
}

export default SearchList;
