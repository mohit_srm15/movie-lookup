import {ACTIONS} from './Action';

export function setLoading(payload) {
  return {
    type: ACTIONS.LOADING,
    payload: payload,
  };
}

export function addShortlistData(payload) {
  return {
    type: ACTIONS.ADD_SHORTLIST_DATA,
    payload: payload,
  };
}
