import React from 'react';
import {createAppContainer} from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';
import {createBottomTabNavigator} from 'react-navigation-tabs';
import Icon from 'react-native-vector-icons/Feather';

import Splash from './Screens/Splash';
import Search from './Screens/Search';
import Shortlist from './Screens/Shortlist';
import Color from './Styles/Color';

const bottomTabNavigator = createBottomTabNavigator(
  {
    Search: {
      screen: Search,
      navigationOptions: {
        tabBarIcon: ({tintColor}) => {
          return (
            <Icon
              name={'search'}
              size={25}
              color={tintColor}
              style={{justifyContent: 'flex-end'}}
            />
          );
        },
      },
    },
    Shortlist: {
      screen: Shortlist,
      navigationOptions: {
        tabBarIcon: ({tintColor}) => {
          return (
            <Icon
              name={'list'}
              size={25}
              color={tintColor}
              style={{justifyContent: 'flex-end'}}
            />
          );
        },
      },
    },
  },
  {
    initialRouteName: 'Search',
    tabBarOptions: {
      showLabel: false,
      activeTintColor: Color.appColor,
      inactiveTintColor: '#B4BECC',
      showIcon: true,
      tabBarPosition: 'bottom',
      iconStyle: {
        width: 25,
        height: 25,
      },
      tabStyle: {
        borderRadius: 20,
        margin: 10,
      },
      style: {
        backgroundColor: 'black',
        alignItems: 'center',
        justifyContent: 'center',
        alignSelf: 'center',
        borderTopColor: 'black',
        borderTopWidth: 1,
      },
      indicatorStyle: '#fff',
    },
  },
);

const appNavigations = createStackNavigator(
  {
    Splash: {screen: Splash},
    Search: {screen: Search},
    Shortlist: {screen: Shortlist},
    bottomTabNavigator: {screen: bottomTabNavigator},
  },
  {
    headerMode: 'none',
  },
);

export const AppContainer = createAppContainer(appNavigations);
