import React, {useState, useEffect} from 'react';
import {View, TextInput, Platform, KeyboardAvoidingView} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import Icon from 'react-native-vector-icons/AntDesign';

import {BoldText} from '../Components/Common/StyledTexts';
import Color from '../Styles/Color';
import SearchList from '../Components/Search';
import {addShortlistData} from '../Actions/ActionCreator';
import store from '../Store';

function Search() {
  const [searchKeyword, setSearchKeyword] = useState('');
  useEffect(() => {
    getStoredShortlist();
  }, [0]);

  async function getStoredShortlist() {
    let movieList = await AsyncStorage.getItem('shortlistData');
    if (movieList && Object.keys(movieList)) {
      store.dispatch(addShortlistData(JSON.parse(movieList)));
    }
  }

  return (
    <View style={{flex: 1, backgroundColor: Color.black}}>
      <View
        style={{
          flex: Platform.OS == 'ios' ? 0.1 : 0.07,
          justifyContent: 'flex-end',
          alignItems: 'center',
          paddingBottom: 10,
          backgroundColor: Color.gray06,
        }}>
        <BoldText
          style={{
            color: Color.headerColor,
            fontWeight: '600',
            fontSize: 25,
            letterSpacing: 2,
          }}>
          Look Up !
        </BoldText>
      </View>
      <View style={{flex: 0.07}}>
        <KeyboardAvoidingView
          behavior="position"
          keyboardVerticalOffset={-0}
          style={{flex: 1}}>
          <View
            style={{
              justifyContent: 'space-between',
              alignItems: 'flex-start',
              paddingHorizontal: 20,
              flexDirection: 'row',
              backgroundColor: Color.gray04,
              borderBottomLeftRadius: 30,
              borderBottomRightRadius: 30,
              height: '100%',
            }}>
            <View
              style={{
                height: '100%',
                justifyContent: 'center',
                width: '90%',
              }}>
              <TextInput
                placeholder={'Search'}
                placeholderTextColor={'white'}
                onChangeText={(value) => setSearchKeyword(value)}
                value={searchKeyword}
                style={{
                  color: 'white',
                  fontSize: 20,
                  justifyContent: 'center',
                  alignSelf: 'center',
                  width: '90%',
                }}
                editable={true}
              />
            </View>
            <View
              style={{
                justifyContent: 'center',
                alignItems: 'center',
                alignSelf: 'center',
              }}>
              <Icon name={'search1'} size={20} color={'white'} />
            </View>
          </View>
        </KeyboardAvoidingView>
      </View>
      <SearchList searchKeyword={searchKeyword} />
    </View>
  );
}

export default Search;
