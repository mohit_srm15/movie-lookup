import React, {useState, useEffect} from 'react';
import {Platform, ScrollView, View} from 'react-native';
import {connect} from 'react-redux';

import Color from '../Styles/Color';
import {BoldText, LightText} from '../Components/Common/StyledTexts';
import VideoContainer from '../Components/Common/VideoContainer';
import AsyncStorage from '@react-native-community/async-storage';

function Shortlist(props) {
  const [movieList, setMovieList] = useState({});

  useEffect(() => {
    if (
      props.state &&
      props.state.shortList &&
      Object.keys(props.state.shortList)
    ) {
      setMovieList({...props.state.shortList});
    }
  }, [props]);

  useEffect(() => {
    getStoredShortlist();
  }, [0]);

  async function getStoredShortlist() {
    let movieList = await AsyncStorage.getItem('shortlistData');
    if (movieList && Object.keys(movieList)) {
      setMovieList(JSON.parse(movieList));
    }
  }

  return (
    <View style={{flex: 1, backgroundColor: Color.black}}>
      <View
        style={{
          flex: Platform.OS == 'ios' ? 0.1 : 0.07,
          justifyContent: 'flex-end',
          alignItems: 'center',
          paddingBottom: 10,
          backgroundColor: Color.gray06,
        }}>
        <BoldText
          style={{
            color: Color.headerColor,
            fontWeight: '600',
            fontSize: 25,
            letterSpacing: 2,
          }}>
          Shortlist
        </BoldText>
      </View>
      <ScrollView style={{flex: 1, padding: 10, marginVertical: 10}}>
        <View style={{flexDirection: 'row', flexWrap: 'wrap'}}>
          {Object.keys(movieList).length > 0 ? (
            Object.keys(movieList).map((movie, i) => (
              <View
                key={`movie-${i}`}
                style={{width: '50%', borderWidth: 1, paddingHorizontal: 10}}>
                <VideoContainer movieDetail={movieList[movie]} />
              </View>
            ))
          ) : (
            <View style={{padding: 20}}>
              <LightText>No movie shortlisted yet</LightText>
            </View>
          )}
        </View>
      </ScrollView>
    </View>
  );
}

const mapStateToProps = (state) => {
  return {state};
};

Shortlist = connect(mapStateToProps)(Shortlist);
export default Shortlist;
