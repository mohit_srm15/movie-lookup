import Axios from 'axios';

import {CONSTANTS} from './Constants/Constant';

export async function searchMovie(searchKeyword) {
  try {
    if (searchKeyword && searchKeyword.split('').length >= 4) {
      let searchResp = await Axios.get(`${CONSTANTS.URL.BASE_URL}`, {
        params: {
          type: CONSTANTS.TYPES.MOVIE,
          apikey: CONSTANTS.CONFIG.API_KEY,
          s: searchKeyword,
        },
      });
      if (searchResp.data && searchResp.data.Search) {
        return searchResp.data.Search;
      }
      if (searchResp.data.Error) {
        return [];
      }
    }
    return [];
  } catch (error) {
    console.log('search  error  ', error);
  }
}
