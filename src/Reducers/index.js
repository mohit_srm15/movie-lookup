import {ACTIONS} from '../Actions/Action';

const initialState = {
  userData: {},
  loading: false,
};

export function rootReducer(state = initialState, action) {
  if (action.type === ACTIONS.ADD_SHORTLIST_DATA) {
    state = {...state, shortList: action.payload};
  }
  if (action.type === ACTIONS.LOADING) {
    state = {...state, loading: action.payload};
  }
  return state;
}
