export const CONSTANTS = {
  CONFIG: {
    API_KEY: 'a1b5f9ec',
  },
  URL: {
    BASE_URL: 'http://www.omdbapi.com/',
  },
  TYPES: {
    MOVIE: 'movie',
  },
};
